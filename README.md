# Candy sorter

This project is a try to implement candy sorting with JETANK robot.

The ML model classifies candies into 7 types. Types that correspond to classes 0,1,2,3 are good candies. Classes 4,5,6 are licorice candies. The robot should pick up candies in front of it, and put them to corresponding boxes: one box for the good candies, and the second box is for the good candies.

The model (based on pretrained ResNet18) was trained on 350 images (50 for each class - divided to 36 images for train set and 14 for test set). The amount epochs: 60. The best weights for the model are saved into ``best_model_resnet18.pth`` file. 

## Usage 

To reproduce, build and set up the JETANK robot as done [here](https://www.youtube.com/watch?v=qNy1hulFk6I&t=472s). 
Install [dependencies](## Dependencies).
Run cells in the ```sorting``` notebook one by one. Read the instructions and explanations in the notebook.

Before running the project, tie 2 boxes behind the JETANK, and put candies in front of it on white surface like on picture: 

![pic](installation.jpg)

> Use only candies like on the picture, otherwise the sorting can be wrong. Use good lighting, minimize shades. Do not leave the robot unattended. **Note: This document include servos control. When running the code, be careful if there are fragile objects in the range of motion of the robot arm, and keep it away from children.**

## Dependencies

The project is based on the  [jetbot JETSON NANO 4 GB image](https://jetbot.org/master/software_setup/sd_card.html). To reproduce, build and set up the JETANK robot as done [here](https://www.youtube.com/watch?v=qNy1hulFk6I&t=472s). 

* [Nanocamera](https://github.com/thehapyone/NanoCamera) - A simple to use camera interface for the Jetson Nano for working with USB, CSI, IP and also RTSP cameras or streaming video in Python
* [Opencv](https://opencv.org/) - Used to operate on images
* [Numpy](https://numpy.org/) - Scientific computing in Python
* [Matplotlib](https://matplotlib.org/) - Visualization with Python
* [jetbot](https://github.com/NVIDIA-AI-IOT/jetbot) - An educational AI robot based on NVIDIA Jetson Nano.
* SCSCtrl - library to control the servos (should be on your JETANK image)

To install matplotlib, run ```pip3 install matplotlib```
To install nanocamera, run ```pip3 install nanocamera```

Other dependencies should be pre-instlled on the [jetbot JETSON NANO 4 GB image](https://jetbot.org/master/software_setup/sd_card.html).

## The algorithm

The main logic of the program is a loop that does the following things:
 * take an image
 * detect a candy on the image
 * if candy is detected - 
     * classify it
     * grab the candy
     * put the candy into a box depending on the result of the classification. 
 * else - scan the surface.

The loop is ended when robot goes a certain amount of steps forward while scanning the surface.

## The demo

[Candy sorter](https://youtu.be/9jB0tTmY0V0)

## Maintainers
@MariaNema

