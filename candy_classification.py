import torch
import torchvision
import cv2
import PIL.Image
import torchvision.transforms as transforms
import torch.nn.functional as F
import numpy as np

CANDY_DICT = ['long red black' , 'long red red', 'round red', 
             'sour yellow', 'licorice boat', 'licorice sponge', 'licorice star']

mean = torch.Tensor([0.485, 0.456, 0.406]).cuda().half()
std = torch.Tensor([0.229, 0.224, 0.225]).cuda().half()
normalize = torchvision.transforms.Normalize(mean, std)
model = torchvision.models.resnet18(pretrained=False)
model.fc = torch.nn.Linear(512, 7)
model.load_state_dict(torch.load('best_model_resnet18.pth'))
device = torch.device('cuda')
model = model.to(device)
model = model.eval().half()

def preprocess(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = cv2.resize(img, dsize=(224, 224), interpolation=cv2.INTER_CUBIC)
    img = PIL.Image.fromarray(img)
    img = transforms.functional.to_tensor(img).to(device).half()
    img.sub_(mean[:, None, None]).div_(std[:, None, None])
    return img[None, ...]

def classifyCandy(img):
    x = preprocess(img)
    y = model(x)
    y = F.softmax(y, dim=1)
    prob = [t.detach().cpu().numpy() for t in y][0]
    i = np.unravel_index(np.argmax(prob, axis=None), prob.shape)[0]
    return (i, max(prob))
